;;; completion/vertico-postframe/config.el -*- lexical-binding: t; -*-

(use-package! vertico-posframe
  :after vertico
  :config
  (setq vertico-posframe-height 40
        vertico-posframe-width 120
        vertico-count 38)
  (vertico-posframe-mode 1))
