;;; ui/org-modern/config.el -*- lexical-binding: t; -*-

(use-package! org-modern
  :hook ((org-mode org-agenda-finalize) . org-modern-mode))

(after! org
  :config
  (setq org-startup-indented nil
        org-auto-align-tags nil
        org-tags-column 0
        org-fold-catch-invisible-edits 'show-and-error
        org-insert-heading-respect-content t
        org-highlight-latex-and-related '(native)
        org-ellipsis "⤵"
        org-startup-indented nil))


(add-hook 'org-mode-hook '+org-pretty-mode)
