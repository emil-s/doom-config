;;; ui/zen-lite/config.el -*- lexical-binding: t; -*-

(use-package! mixed-pitch
  :hook ((adoc-mode rst-mode markdown-mode org-mode) . mixed-pitch-mode)
  :config
  (pushnew! mixed-pitch-fixed-pitch-faces
            'solaire-line-number-face
            'org-date
            'org-footnote
            'org-special-keyword
            'org-property-value
            'org-ref-cite-face
            'org-tag
            'org-todo-keyword-todo
            'org-todo-keyword-habt
            'org-todo-keyword-done
            'org-todo-keyword-wait
            'org-todo-keyword-kill
            'org-todo-keyword-outd
            'org-todo
            'org-done
            ;; 'tree-sitter-hl-face
            'font-lock-comment-face))


;; (add-hook! 'mixed-pitch-mode-hook :append
;;   (text-scale-set 1.2))
