;; -*- no-byte-compile: t; -*-
;;; tools/eaf/packages.el

(package! eaf :recipe
  (:host github
   :repo "manateelazycat/emacs-application-framework"
   :files ("*.el" "*.py" "app" "core")
   :build (:not compile)))

(package! ctable :recipe (:host github :repo "kiwanami/emacs-ctable"))
(package! deferred :recipe (:host github :repo "kiwanami/emacs-deferred"))
(package! epc :recipe (:host github :repo "kiwanami/emacs-epc"))
