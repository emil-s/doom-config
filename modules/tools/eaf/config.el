;;; tools/eaf/config.el -*- lexical-binding: t; -*-

(use-package! eaf
  :commands (eaf-open-browser eaf-open find-file)
  :config
  (use-package! ctable)
  (use-package! deferred)
  (use-package! epc))
