;;; lang/roam2-ui/config.el -*- lexical-binding: t; -*-

(use-package! websocket
  :after org-roam)

(use-package! org-roam-ui
  :after org-roam ;; or :after org
  ;;         normally we'd recommend hooking orui after org-roam, but since org-roam does not have
  ;;         a hookable mode anymore, you're advised to pick something yourself
  ;;         if you don't care about startup time, use
  ;;  :hook (after-init . org-roam-ui-mode)

  ;; :init
  ;; (when (featurep 'xwidget-internal)
  ;;   (setq org-roam-ui-browser-function #'xwidget-webkit-browse-url))


  :config
  (setq org-roam-ui-sync-theme t
        org-roam-ui-update-on-save t))

(after! org-roam
  (map! :map org-mode-map
        :localleader
        "u" #'org-roam-ui-open))
