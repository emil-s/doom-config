;;; lang/config.el -*- lexical-binding: t; -*-

(use-package! sqlup-mode
  :hook (sql-mode . sqlup-mode)
  :config
  (map! :map sql-mode-map
        :localleader
        :desc "Send buffe"                 "e" #'sql-send-buffer
        :desc "Send region"                "E" #'sql-send-region
        :desc "Send line and next"         "l" #'sql-send-line-and-next
        :desc "Connect to postgresql"      "p" #'sql-postgres
        :desc "List postgresql databases"  "l" #'sql-postgres-list-databases))

(use-package! sql-indent
  :hook (sql-mode . sqlind-minor-mode))

(when (featurep! +lsp)
  (add-hook 'sql-mode-hook #'lsp! 'append)
  (after! lsp-mode
    (setq lsp-sqls-workspace-config-path nil)))
