;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

(setq user-full-name "Emil"
      user-mail-address "emil@emil"
      auth-sources '("~/.authinfo.gpg")
      auth-source-cache-expiry nil)

(setq doom-gruvbox-material-background "medium"
      doom-gruvbox-material-palette "mix"

      doom-gruvbox-material-light-background "medium"
      doom-gruvbox-material-light-palette "mix"

      doom-gruvbox-material-brighter-comments t
      doom-gruvbox-material-brighter-modeline nil)

(setq doom-theme 'doom-gruvbox-material)

(setq doom-font
      (font-spec :family "JuliaMono" :size 13 :weight 'regular)

      doom-big-font
      (font-spec :family "JuliaMono" :size 22 :weight 'regular)

      ;; doom-unicode-font
      ;; (font-spec :family "JuliaMono" :size 14)

      doom-variable-pitch-font
      (font-spec :family "Lato" :size 13 :weight 'regular)

      ;; all-the-icons-scale-factor 1.1
      line-spacing 0.2)

(custom-set-faces!
  '(rust-string-interpolation :inherit font-lock-variable-name-face
                                   :slant italic)
  '(lsp-rust-analyzer-inlay-face :inherit font-lock-comment-face
                                 :slant italic)
  '(lsp-javascript-inlay-face :inherit font-lock-comment-face
                              :slant italic))

(after! web-mode (setq web-mode-prettify-symbols-alist nil))

;; (map! (:map rustic-mode-map
;;        :localleader
;;        :desc "Toggle LSP hints" "h" #'lsp-rust-analyzer-inlay-hints-mode))

(setq display-line-numbers-type nil)

(setq fancy-splash-image "~/.doom.d/astolfomacs.png")

(setq-default uniquify-buffer-name-style 'post-forward-angle-brackets)

(setq-default delete-by-moving-to-trash t)

(setq scroll-preserve-screen-position 'always
      scroll-margin 6)

(when (string= system-type "darwin")
  (pixel-scroll-precision-mode t))

(setq doom-modeline-height 14)
(after! doom-modeline
  (doom-modeline-def-modeline 'main
    '(bar modals vcs matches workspace-name window-number checker process remote-host)
    '(process buffer-info buffer-position indent-info " ")))

(after! evil
  (setq evil-ex-substitute-global t
        evil-move-cursor-back nil
        evil-kill-on-visual-paste nil))



(after! org
  (plist-put org-format-latex-options :scale 2)
  (custom-set-faces!
    '((org-block org-block-begin-line org-block-end-line) :background nil)))

(setq org-directory "~/Documents/org/"
      org-preview-latex-default-process 'dvisvgm
      org-latex-inputenc-alist '(("utf8" . "utf8x")))

(after! markdown-mode
  (custom-set-faces!
    '((markdown-pre-face, markdown-code-face) :background nil)))

(after! lsp-rust
  (setq lsp-rust-analyzer-lru-capacity 100
        lsp-rust-analyzer-server-display-inlay-hints t
        lsp-rust-analyzer-display-chaining-hints t
        lsp-rust-analyzer-display-reborrow-hints t
        lsp-rust-analyzer-display-closure-return-type-hints t
        lsp-rust-analyzer-display-parameter-hints t
        lsp-rust-analyzer-display-lifetime-elision-hints-enable "skip_trivial"
        lsp-rust-analyzer-display-lifetime-elision-hints-use-parameter-names t
        lsp-rust-analyzer-cargo-watch-enable t
        lsp-rust-analyzer-cargo-run-build-scripts t
        lsp-rust-analyzer-proc-macro-enable t
        lsp-rust-analyzer-cargo-watch-command "clippy"

        lsp-clients-angular-language-server-command
        '("node"
          "node-modules/@angular/language-server"
          "--ngProbeLocations"
          "node_modules/"
          "--tsProbeLocations"
          "node_modules/"
          "--stdio"))
  (require 'dap-cpptools))

(after! lsp-mode
  (setq lsp-idle-delay 0.4
        lsp-headerline-breadcrumb-enable t
        lsp-headerline-breadcrumb-icons-enable nil
        lsp-headerline-breadcrumb-segments '(file symbols)))

(map! :after emmet-mode [tab] nil)

(after! dap-mode
  (setq dap-default-terminal-kind "integrated")
  (dap-auto-configure-mode +1))

(after! dap-cpptools
  (dap-register-debug-template "Rust::CppTools Run Configuration"
                               (list :type "cppdbg"
                                     :request "launch"
                                     :name "Rust::Run"
                                     :MIMode "gdb"
                                     :miDebuggerPath "rust-gdb"
                                     :environment []
                                     :program "${workspaceFolder}/target/debug/binary"
                                     :cwd "${workspaceFolder}"
                                     :console "external"
                                     :dap-compilation "cargo build"
                                     :dap-compilation-dir "${workspaceFolder}")))

(add-hook 'pdf-view-mode-hook
          (lambda ()
            (setq left-fringe-width 0
                  right-fringe-width 0)
            (hide-mode-line-mode)
            (pdf-view-midnight-minor-mode)))
